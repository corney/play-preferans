/**
 * Created by dmitriikariaev on 5/23/16.
 */
$(document).ready(function () {
    $.get({
        url: "/persons",
        success: function(persons) {
            $.each(persons, function(index, person) {

                var name = $("<div>").addClass("name").text(person.name);
                console.log("name", name)
                $("#persons").append($("<li>").append(name));
            })
        }
    })
});