package ru.corney.preferans.actor

import akka.actor.{ActorSystem, Props}
import akka.testkit._
import org.specs2._
import org.specs2.execute.{AsResult, Success}
import ru.corney.preferans.actor.BidMachine._
import ru.corney.preferans.game.Place._
import ru.corney.preferans.game.Suit._
import ru.corney.preferans.game._



/**
  * Created by dmitriikariaev on 6/1/16.
  */
class BidMachineWithHereAllowedSpec extends TestKit(ActorSystem("bidmachine-test")) with DefaultTimeout with ImplicitSender with mutable.SpecificationLike {

  sequential

  implicit val convention = Convention(AllPassConvention(dealerIsAllowedToGetTricks = true), BidConvention(allowHere = true))

  "BidMachine with allowed 'here'" should {


    val bidMachine = TestActorRef(Props(new BidMachine(WEST, Set(NORTH, EAST, SOUTH))))

    "allows correct bids" in {
      bidMachine ! Bid(NORTH, TrickContract(6, Some(SPADES)))
      expectMsg(AskForBid(EAST))
      bidMachine ! Bid(EAST, TrickContract(6, Some(CLUBS)))
      expectMsg(AskForBid(SOUTH))
      bidMachine ! Bid(SOUTH, TrickContract(6, Some(DIAMONDS)))
      expectMsg(AskForBid(NORTH))
      bidMachine ! Bid(NORTH, TrickContract(6, Some(HEARTS)))
      expectMsg(AskForBid(EAST))
      bidMachine ! Bid(EAST, TrickContract(6, None))
      expectMsg(AskForBid(SOUTH))
      bidMachine ! Bid(SOUTH, TrickContract(7, Some(SPADES)))
      expectMsg(AskForBid(NORTH))
    }

    "disallows incorrect bids" in {
      bidMachine ! Bid(EAST, TrickContract(7, Some(CLUBS)))
      expectMsg(WrongHandSequence)
      bidMachine ! Bid(SOUTH, TrickContract(7, Some(CLUBS)))
      expectMsg(WrongHandSequence)
      bidMachine ! Bid(NORTH, TrickContract(7, Some(SPADES)))
      expectMsg(CantOverride)
      bidMachine ! Bid(NORTH, TrickContract(7, Some(CLUBS)))
      expectMsg(AskForBid(EAST))
    }

    "allows 'here' bid" in {
      bidMachine ! Bid(EAST, PassContract)
      expectMsg(AskForBid(SOUTH))
      bidMachine ! Bid(SOUTH, TrickContract(7, Some(CLUBS)))
      expectMsg(AskForBid(NORTH))
    }

    "Asking for whisting after declaring game" in {
      bidMachine ! Bid(NORTH, PassContract)
      expectMsg(AskForWhisting(NORTH, Set(PassWhist, Whist)))

      bidMachine ! WhistBid(NORTH, PassWhist)
      expectMsg(AskForWhisting(EAST, Set(PassWhist, Whist, HalfWhist)))

      bidMachine ! WhistBid(EAST, HalfWhist)
      expectMsg(AskForWhisting(NORTH, Set(PassWhist, Whist)))

      bidMachine ! WhistBid(NORTH, PassWhist)
      expectMsg(
        Finished(
          WhistFullGame(
            SOUTH,
            Map(NORTH -> PassWhist, EAST -> HalfWhist),
            TrickContract(7, Some(CLUBS))
          )
        )
      )
    }

    "Stop BidMacnhie" in {
      bidMachine.stop()
    }
  }
  "BidMachine with allowed 'here' also " should {
    "Declare AllPass Game" in {
      val bidMachine = TestActorRef(Props(new BidMachine(WEST, Set(NORTH, EAST, SOUTH))))

      bidMachine ! Bid(NORTH, PassContract)
      expectMsg(AskForBid(EAST))

      bidMachine ! Bid(EAST, PassContract)
      expectMsg(AskForBid(SOUTH))

      bidMachine ! Bid(SOUTH, PassContract)
      expectMsg(Finished(WhistLessGame(WEST, PassContract)))
    }

    "Declare Miser Game" in {
      val bidMachine = TestActorRef(Props(new BidMachine(WEST, Set(NORTH, EAST, SOUTH))))

      bidMachine ! Bid(NORTH, TrickContract(6, Some(SPADES)))
      expectMsg(AskForBid(EAST))

      bidMachine ! Bid(EAST, MisereContract)
      expectMsg(AskForBid(SOUTH))

      bidMachine ! Bid(SOUTH, TrickContract(8, Some(SPADES)))
      expectMsg(CantOverride)

      bidMachine ! Bid(SOUTH, TrickContract(9, Some(SPADES)))
      expectMsg(AskForBid(NORTH))

      bidMachine ! Bid(NORTH, PassContract)
      expectMsg(AskForBid(EAST))

      bidMachine ! Bid(EAST, TrickContract(9, Some(CLUBS)))
      expectMsg(BidNotAllowed)

      bidMachine ! Bid(EAST, MisereContract)
      expectMsg(BidNotAllowed)

      bidMachine ! Bid(EAST, TalonlessMisereContract)
      expectMsg(AskForBid(SOUTH))

      bidMachine ! Bid(SOUTH, TrickContract(9, Some(DIAMONDS)))
      expectMsg(CantOverride)

      bidMachine ! Bid(SOUTH, TalonlessTrickContract(9, Some(DIAMONDS)))
      expectMsg(AskForBid(EAST))

      bidMachine ! Bid(EAST, PassContract)
      expectMsg(AskForWhisting(NORTH, Set(PassWhist, Whist)))

      bidMachine ! WhistBid(NORTH, Whist)
      expectMsg(AskForWhisting(EAST, Set(PassWhist, Whist)))

      bidMachine ! WhistBid(EAST, PassWhist)
      expectMsg(
        Finished(
          WhistFullGame(
            SOUTH,
            Map(NORTH -> Whist, EAST -> PassWhist),
            TalonlessTrickContract(9, Some(DIAMONDS))
          )
        )
      )


    }

    "Declare Game after two pass" in {
      val bidMachine = TestActorRef(Props(new BidMachine(WEST, Set(NORTH, EAST, SOUTH))))

      bidMachine ! Bid(NORTH, PassContract)
      expectMsg(AskForBid(EAST))

      bidMachine ! Bid(EAST, PassContract)
      expectMsg(AskForBid(SOUTH))

      bidMachine ! Bid(SOUTH, TrickContract(6, Some(SPADES)))
      expectMsg(AskForWhisting(NORTH, Set(Whist, PassWhist)))

      bidMachine ! WhistBid(NORTH, Whist)
      expectMsg(AskForWhisting(EAST, Set(PassWhist, Whist)))


      bidMachine ! WhistBid(EAST, PassWhist)
      expectMsg(
        Finished(
          WhistFullGame(
            SOUTH,
            Map(NORTH -> Whist, EAST -> PassWhist),
            TrickContract(6, Some(SPADES))
          )
        )
      )
    }

    "Declare Game on first hand " in {
      val bidMachine = TestActorRef(Props(new BidMachine(WEST, Set(NORTH, EAST, SOUTH))))

      bidMachine ! Bid(NORTH, TrickContract(6, Some(SPADES)))
      expectMsg(AskForBid(EAST))

      bidMachine ! Bid(EAST, PassContract)
      expectMsg(AskForBid(SOUTH))

      bidMachine ! Bid(SOUTH, PassContract)
      expectMsg(AskForWhisting(EAST, Set(Whist, PassWhist)))

      bidMachine ! WhistBid(EAST, Whist)
      expectMsg(AskForWhisting(SOUTH, Set(PassWhist, Whist)))


      bidMachine ! WhistBid(SOUTH, PassWhist)
      expectMsg(
        Finished(
          WhistFullGame(
            NORTH,
            Map(EAST -> Whist, SOUTH -> PassWhist),
            TrickContract(6, Some(SPADES))
          )
        )
      )
    }


    "Declare Game on second hand " in {
      val bidMachine = TestActorRef(Props(new BidMachine(NORTH, Set(EAST, SOUTH, WEST))))

      bidMachine ! Bid(EAST, PassContract)
      expectMsg(AskForBid(SOUTH))

      bidMachine ! Bid(SOUTH, TrickContract(6, Some(SPADES)))
      expectMsg(AskForBid(WEST))

      bidMachine ! Bid(WEST, PassContract)
      expectMsg(AskForWhisting(WEST, Set(Whist, PassWhist)))

      bidMachine ! WhistBid(WEST, Whist)
      expectMsg(AskForWhisting(EAST, Set(PassWhist, Whist)))

      bidMachine ! WhistBid(EAST, PassWhist)
      expectMsg(
        Finished(
          WhistFullGame(
            SOUTH,
            Map(WEST -> Whist, EAST -> PassWhist),
            TrickContract(6, Some(SPADES))
          )
        )
      )
    }
  }

  implicit def anyToSuccess[T]: AsResult[T] = new AsResult[T] {
    def asResult(t: => T) = {
      t
      Success()
    }
  }

}
