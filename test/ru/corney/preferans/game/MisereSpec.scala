package ru.corney.preferans.game

import org.scalatestplus.play._
import ru.corney.preferans.game.Place._
import ru.corney.preferans.game.Rank._
import ru.corney.preferans.game.Suit._

import scala.util.{Failure, Success}

/**
  * Created by dmitriikariaev on 5/26/16.
  */
class MisereSpec extends PlaySpec with OneAppPerSuite {

  val west = Set(
    Card(SPADES, NINE),
    Card(SPADES, ACE),
    Card(HEARTS, QUEEN),
    Card(SPADES, JACK),
    Card(CLUBS, TEN),
    Card(DIAMONDS, NINE),
    Card(DIAMONDS, ACE),
    Card(DIAMONDS, JACK),
    Card(CLUBS, ACE),
    Card(CLUBS, SEVEN)
  )
  val north = Set(
    Card(SPADES, SEVEN),
    Card(HEARTS, SEVEN),
    Card(CLUBS, KING),
    Card(CLUBS, EIGHT),
    Card(CLUBS, JACK),
    Card(DIAMONDS, KING),
    Card(HEARTS, NINE),
    Card(SPADES, EIGHT),
    Card(DIAMONDS, EIGHT),
    Card(SPADES, KING)
  )

  val east = Set(
    Card(DIAMONDS, QUEEN),
    Card(CLUBS, QUEEN),
    Card(HEARTS, JACK),
    Card(HEARTS, EIGHT),
    Card(SPADES, TEN),
    Card(HEARTS, KING),
    Card(HEARTS, ACE),
    Card(HEARTS, TEN),
    Card(DIAMONDS, TEN),
    Card(CLUBS, NINE)
  )

  val talon = Seq(
    Card(SPADES, QUEEN),
    Card(DIAMONDS, SEVEN)
  )

  "Misere" should {

    val game = Misere(SOUTH, NORTH, Map(WEST -> west, NORTH -> north, EAST -> east))

    "EAST takes first trick" in {
      game.leading mustBe WEST

      val result = game.round(
        Map(
          (WEST, Card(SPADES, NINE)),
          (NORTH, Card(SPADES, SEVEN)),
          (EAST, Card(SPADES, TEN))
        )
      )

      result mustBe Success(EAST)
      game.leading mustBe EAST
    }

    "Step with absent card fails" in {
      val result = game.round(
        Map(
          (WEST, Card(DIAMONDS, NINE)),
          (NORTH, Card(DIAMONDS, KING)),
          (EAST, Card(DIAMONDS, SEVEN))
        )
      )

      result mustBe a [Failure[_]]
    }

    "Step with wrong suit fails" in {
      val result = game.round(
        Map(
          (WEST, Card(DIAMONDS, NINE)),
          (NORTH, Card(DIAMONDS, KING)),
          (EAST, Card(CLUBS, QUEEN))
        )
      )

      result mustBe a [Failure[_]]
    }

    "North takes a trick in the second step" in {
      val result = game.round(
        Map(
          (WEST, Card(DIAMONDS, NINE)),
          (NORTH, Card(DIAMONDS, KING)),
          (EAST, Card(DIAMONDS, QUEEN))
        )
      )

      result mustBe Success(NORTH)
      game.leading mustBe NORTH
    }

    "North takes a trick in the third step" in {
      val result = game.round(
        Map(
          (WEST, Card(SPADES, JACK)),
          (NORTH, Card(SPADES, KING)),
          (EAST, Card(CLUBS, QUEEN))
        )
      )

      result mustBe Success(NORTH)
      game.leading mustBe NORTH
    }

    "WEST takes a trick in the forth step" in {
      val result = game.round(
        Map(
          (NORTH, Card(SPADES, EIGHT)),
          (EAST, Card(CLUBS, NINE)),
          (WEST, Card(SPADES, ACE))
        )
      )

      result mustBe Success(WEST)
      game.leading mustBe WEST
      val tricks = game.state.tricks

      tricks(NORTH) mustBe 2
      tricks(EAST) mustBe 1
      tricks(WEST) mustBe 1

    }

  }
}
