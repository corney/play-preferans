package ru.corney.preferans.game

import org.scalatestplus.play._
import play.api.test._
import Place._
/**
  * Created by dmitriikariaev on 5/25/16.
  */
class CardSpec extends PlaySpec with OneAppPerSuite {

  "Deck" should {
    "Have all cards in right order" in {
      val cards = Deck.Cards
      cards must have size 32
    }

    "Deal cards" in {
      val deal = Deal.deal(WEST, Set(NORTH, SOUTH, EAST))
      deal(NORTH) must have size 10
      deal(EAST) must have size 10
      deal(SOUTH) must have size 10
      deal.talon must have size 2

      println("Left hand")
      deal(NORTH) foreach println
      println("\nCenter")
      deal(EAST) foreach println
      println("\nRight hand")
      deal(SOUTH) foreach println
      println("\nTalon")
      deal.talon foreach println
    }

    "Player next" in {
      Place.next(NORTH) mustBe EAST
      Place.next(EAST) mustBe  SOUTH
      Place.next(SOUTH) mustBe  WEST
      Place.next(WEST) mustBe  NORTH

    }
  }
}
