package ru.corney.preferans.dao

import org.scalatest._
import org.scalatestplus.play._
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test._
import play.api.test.Helpers._
import reactivemongo.bson.BSONValue
import ru.corney.preferans.model.Person

import scala.concurrent.Future

/**
  * Created by dmitriikariaev on 6/24/16.
  */

class PersonRepoSpec extends PlaySpec with OneAppPerSuite {

  override implicit lazy val app = new GuiceApplicationBuilder()
    .configure("mongodb.uri" -> "mongodb://localhost:27017/preferans_test")
    .build()

  "PersonRepo" should {
    "Insert person and than read, update and delete it" in {
      val repo: PersonRepo = app.injector.instanceOf[PersonRepo]
      val person = Person(Some("1"), "testLogin", "testPassword", "Test Test")
      val result = await(repo.save(person))

      result mustEqual person

      val result1 = await(repo.find("1"))
      result1.get mustEqual person

      val update = person.copy(name = "Test Updated")
      val result2 = await(repo.save(update))

      result2 mustEqual update

      val result3 = await(repo.find("1"))
      result3.get mustEqual update

      val result4 = await(repo.delete(update))

      val result5 = await(repo.find("1"))
      result5 mustBe None

    }

    "Insert person without ID" in {
      val repo: PersonRepo = app.injector.instanceOf[PersonRepo]
      val person = Person(None, "testLogin", "testPassword", "Test Test")
      val result = await(repo.save(person))

      val result1 = await(repo.find(result.id.get))
      result1.get mustEqual result

      val result2 = await(repo.delete(result))
    }
  }

}
