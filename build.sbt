name := """play-preferans"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
//  jdbc,
  cache,
  ws,
  specs2,
  "org.reactivemongo" %% "play2-reactivemongo" % "0.11.11",
  "com.typesafe.akka" %% "akka-testkit" % "2.4.2" % Test,
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.0" % Test

)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

fork in run := true