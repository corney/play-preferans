package ru.corney.preferans.model

import play.api.data._
import play.api.data.Forms.{ text, longNumber, mapping, nonEmptyText, optional }
import play.api.data.validation.Constraints.pattern

/**
  * Created by dmitriikariaev on 5/20/16.
  */
case class Person(
                   id: Option[String],
                   login: String,
                   password: String,
                   name: String
                 )

object Person {
  import play.api.libs.json._

  val FieldID       = "_id"
  val FieldLogin    = "login"
  val FieldPassword = "password"
  val FieldName     = "name"

  // TODO find a way to make with conversion automatically

  implicit object PersonWrites extends OWrites[Person] {
    def writes(person: Person): JsObject = {
      Json.obj(
        FieldID -> person.id,
        FieldLogin -> person.login,
        FieldPassword -> person.password,
        FieldName -> person.name
      )
    }
  }

  implicit object PersonReads extends Reads[Person] {
    def reads(json: JsValue): JsResult[Person] = json match {
      case obj: JsObject =>
        val id = (obj \ FieldID).asOpt[String]
        val login = (obj \ FieldLogin).as[String]
        val password = (obj \ FieldPassword).as[String]
        val name = (obj \ FieldName).as[String]
        JsSuccess(Person(id, login, password, name))

      case _ => JsError("expected.jsobject")
    }
  }
}
