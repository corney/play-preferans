package ru.corney.preferans.actor

import akka.actor.{ActorLogging, FSM}
import ru.corney.preferans.game.Place.Place
import ru.corney.preferans.game._

/**
  * Created by dmitriikariaev on 5/30/16.
  */
object BidMachine {

  sealed trait State

  sealed trait Data

  sealed trait DeclaredGame extends Data {
    val declarer: Place
    val contract: Contract
  }

  case class UndeclaredGame(nextBidder: Place, bids: Map[Place, Contract], best: Option[(Place, Contract)]) extends Data

  case class WhistLessGame(declarer: Place, contract: Contract) extends DeclaredGame

  case class WhistFullGame(declarer: Place, whisters: Map[Place, WhistContract], contract: Contract) extends DeclaredGame

  // States
  case object WaitForBid extends State

  case object WaitForLeftHandWhisting extends State

  case object WaitForRightHandWhisting extends State

  case object WaitAgainForLeftHandWhisting extends State

  case object Declared extends State

}


import ru.corney.preferans.actor.BidMachine._


// TODO добавить стартовые ограничения (например, торговля начинается только с семерной или восьмерной)
class BidMachine(dealer: Place, players: Set[Place])(implicit convention: Convention) extends FSM[BidMachine.State, BidMachine.Data] with ActorLogging {


  startWith(WaitForBid, UndeclaredGame(next(dealer), Map.empty, None))

  when(WaitForBid) {
    case Event(Bid(place, contract), data: UndeclaredGame) =>

      val (leftHand, rightHand) = opponents(place)

      val allowHere = data.bids.get(rightHand).contains(PassContract) && convention.bidConvention.allowHere

      val nextBidder = if (data.bids.get(leftHand).contains(PassContract)) rightHand else leftHand

      // Эта заявка - последняя и ведет к завершению игры в случае, когда и левая рука, и правая пасанули
      val lastBid = data.bids.get(nextBidder).contains(PassContract)

      // TODO нужно вычислять некст-биддера и, соответственно, есть ли такой вообще, с помощью scala-way

      if (place != data.nextBidder) {
        // Ход не с той руки
        stay replying WrongHandSequence
      } else if (!data.bids.get(place).forall(previous => contract isAllowedAfter previous)) {
        // Запрещено делать данную заявку после предыдущей, например, десятерную нельзя объявлять после мизера,
        // а мизер - после шестерной
        stay replying BidNotAllowed
      } else if (contract == PassContract) {
        // Пользователь пасанул
        if (data.bids.get(nextBidder).contains(PassContract)) {
          val game = WhistLessGame(dealer, PassContract)
          goto(Declared) using game replying Finished(game)
        } else if (data.best.exists { case (bestPlace, _) => bestPlace == nextBidder }) {
          val (bestPlace, bestContract) = data.best.get
          if (bestContract.isWhisterAllowed) {
            val game = WhistFullGame(bestPlace, Map.empty, bestContract)
            val leftWhister = next(bestPlace)
            goto(WaitForLeftHandWhisting) using game replying AskForWhisting(leftWhister, Set(Whist, PassWhist))
          } else {
            val game = WhistLessGame(bestPlace, bestContract)
            goto(Declared) using game replying Finished(game)
          }
        } else {
          stay using
            UndeclaredGame(nextBidder, data.bids.updated(place, contract), data.best) replying
            AskForBid(nextBidder)
        }
      } else if (data.best.forall { case (bestPlace, bestContract) => contract.isBidBetter(bestContract, allowSame = allowHere) }) {
        // Пользователь заявил игру номеналом больше предыдущей
        if (data.bids.get(nextBidder).contains(PassContract)) {
          // Тот, кого мы прочим следующим заявляющим, уже пасанул и эта заявка - самая большая из всех
          if (contract.isWhisterAllowed) {
            val game = WhistFullGame(place, Map.empty, contract)
            goto(WaitForLeftHandWhisting) using game replying AskForWhisting(leftHand, Set(Whist, PassWhist))
          } else {
            val game = WhistLessGame(place, contract)
            goto(Declared) using game replying Finished(game)
          }
        } else {
          stay using
            UndeclaredGame(nextBidder, data.bids.updated(place, contract), Some((place, contract))) replying
            AskForBid(nextBidder)
        }
      } else {
        // Нельзя перебить ставку. остаемся в том же состоянии
        stay replying CantOverride
      }


  }



  when(WaitForLeftHandWhisting) {
    case Event(WhistBid(place, contract), data: WhistFullGame) =>
      val (leftHand, rightHand) = opponents(data.declarer)

      if (place != leftHand) {
        stay replying WrongHandSequence
      } else {
        val allowed: Set[WhistContract] = if (contract == Whist)
          Set(PassWhist, Whist)
        else
          Set(PassWhist, HalfWhist, Whist)

        goto(WaitForRightHandWhisting) using
          WhistFullGame(data.declarer, Map(place -> contract), data.contract) replying
          AskForWhisting(rightHand, allowed)
      }
  }

  when(WaitForRightHandWhisting) {
    case Event(WhistBid(place, contract), data: WhistFullGame) =>
      val (leftHand, rightHand) = opponents(data.declarer)

      if (place != rightHand) {
        stay replying WrongHandSequence
      } else {
        val updated = WhistFullGame(data.declarer, data.whisters.updated(place, contract), data.contract)
        if (contract == HalfWhist) {
          if (data.whisters(leftHand) == PassWhist) {
            goto(WaitAgainForLeftHandWhisting) using updated replying AskForWhisting(leftHand, Set(PassWhist, Whist))
          } else {
            stay replying ShouldPassOrWhist
          }
        } else {
          goto(Declared) using updated replying Finished(updated)
        }
      }
  }

  when(WaitAgainForLeftHandWhisting) {
    case Event(WhistBid(place, contract), data: WhistFullGame) =>
      val (leftHand, rightHand) = opponents(data.declarer)

      if (data.whisters(leftHand) == PassWhist && data.whisters(rightHand) == HalfWhist) {
        val game: WhistFullGame = WhistFullGame(data.declarer, data.whisters.updated(place, contract), data.contract)
        goto(Declared) using game replying Finished(game)
      } else {
        stay replying WrongHalfWhistCombo
      }

  }

  when(Declared) {
    case Event(None, _) =>
      stay()
  }

  def opponents(place: Place): (Place, Place) = {
    val left = next(place)
    val right = next(left)
    (left, right)
  }

  def next(place: Place) = Place.next(place, players)

}

// Запросы
sealed trait BidRequest

case class Bid(place: Place, contract: Contract) extends BidRequest

case class WhistBid(place: Place, contract: WhistContract) extends BidRequest

// Ответы
sealed trait BidResponse

// TODO добавить возврат списка разрешенных ставок
case class AskForBid(place: Place) extends BidResponse

case class AskForWhisting(place: Place, answers: Set[WhistContract]) extends BidResponse

case class Finished(game: DeclaredGame) extends BidResponse


sealed trait BidError extends BidResponse

case object WrongHandSequence extends BidError

case object CantOverride extends BidError

case object BidNotAllowed extends BidError

case object ShouldPassOrWhist extends BidError

case object WrongHalfWhistCombo extends BidError
