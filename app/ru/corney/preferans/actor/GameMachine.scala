package ru.corney.preferans.actor

import akka.actor.FSM
import ru.corney.preferans.game.{Convention, Game}
import ru.corney.preferans.game.Place.Place
import ru.corney.preferans.model.Person

sealed trait GameState
case object UsualBidding extends GameState
case object TrickPlaying extends GameState

/**
  * Created by dmitriikariaev on 5/29/16.
  */
class GameMachine(players: Map[Place, Person], convenction: Convention) extends FSM[GameState, Game]{

  startWith(UsualBidding, Game(players, convenction))
/*
  when(UsualBidding) {
    case
  }*/
}


// Received events
final case class SetConfig()