package ru.corney.preferans.game

import ru.corney.preferans.game.Suit.Suit


/**
  * Created by dmitriikariaev on 6/9/16.
  */

sealed trait WhistContract

case object Whist extends WhistContract
case object HalfWhist extends WhistContract
case object PassWhist extends WhistContract


sealed trait Contract {
  def isWhisterAllowed: Boolean

  /**
    * Проверяет, может ли перебить данная ставка предыдущую
    *
    * @param previous  предыдущая ставка, которую перебиваем
    * @param allowSame разрешено ли перебивать одинаковую ставку
    * @return
    */
  def isBidBetter(previous: Contract, allowSame: Boolean): Boolean

  /**
    * Можно ли сделать данную ставку после совершенной ранее
    *
    * Проверяет, допустима ли данная ставка после совершенной тем же игроком ранее
    *
    * @param previous Ставка, которую игрок совершал до этого
    * @return
    */
  def isAllowedAfter(previous: Contract): Boolean


}

sealed trait DefaultTrickContract extends Contract {
  val tricks: Int
  val trump: Option[Suit]

  def isWhisterAllowed = true

  override def isAllowedAfter(previous: Contract): Boolean = {
    previous match {
      case MisereContract | TalonlessMisereContract | PassContract =>
        false
      case trick: DefaultTrickContract =>
        canOverride(trick, allowSame = false)
    }
  }

  /**
    * Проверяет, перебивает ли проверяемая ставка текущую для случая рядовых игр (не распасы и не мизер).
    * В случае, если число взяток в проверяемой ставке больше, чем в предыдущей, перебивать разрешено.
    * В случае, если число взяток одинаково, сравнивается козырная масть.
    * Если козырная масть одинаковая, или обе игры без козырей, то ставки одинаковы. В этом случае
    * ставка перебивается лишь в случае, когда это допустимо согласно конвенции и расположению игроков.
    *
    * @param previous  Предыдущая ставка
    * @param allowSame Разрешено ли перебивать ставку такой же ставкой
    * @return Перебивает ли проверяемая ставка текущую
    */
  protected def canOverride(previous: DefaultTrickContract, allowSame: Boolean): Boolean = {
    if (tricks == previous.tricks) {
      (trump, previous.trump) match {
        case (Some(cTrump), Some(pTrump)) =>
          cTrump > pTrump || (cTrump == pTrump) && allowSame
        case (None, None) =>
          allowSame
        case (None, _) =>
          true
        case (_, None) =>
          false
      }
    } else {
      tricks > previous.tricks
    }
  }
}

case class TrickContract(tricks: Int, trump: Option[Suit]) extends DefaultTrickContract {
  require(tricks >= 6 && tricks <= 10)

  override def isBidBetter(previous: Contract, allowSame: Boolean) = {
    previous match {
      case tc: TalonlessTrickContract =>
        canOverride(tc, allowSame = false)
      case tc: TrickContract =>
        canOverride(tc, allowSame)
      case MisereContract =>
        tricks > 8
      case _ =>
        false
    }
  }
}

case class TalonlessTrickContract(tricks: Int, trump: Option[Suit]) extends DefaultTrickContract {
  require(tricks == 9 || tricks == 10)

  override def isBidBetter(previous: Contract, allowSame: Boolean) = {
    previous match {
      case tc: TrickContract =>
        canOverride(tc, allowSame = true)
      case tc: TalonlessTrickContract =>
        canOverride(tc, allowSame)
      case MisereContract | TalonlessMisereContract =>
        true
      case PassContract =>
        false
    }
  }
}

case object MisereContract extends Contract {
  override def isWhisterAllowed = false

  override def isBidBetter(previous: Contract, allowSame: Boolean) = {
    previous match {
      case TrickContract(tricks, trumps) =>
        tricks <= 8
      case TalonlessTrickContract(_, _)|TalonlessMisereContract|PassContract =>
        false
      case MisereContract =>
        allowSame
    }
  }

  override def isAllowedAfter(previous: Contract) = false
}

case object TalonlessMisereContract extends Contract {
  override def isWhisterAllowed = false

  override def isBidBetter(previous: Contract, allowSame: Boolean) = {
    previous match {
      case TrickContract(_, _)|MisereContract =>
        true
      case TalonlessTrickContract(_, _)|PassContract =>
        false
      case TalonlessMisereContract =>
        allowSame
    }
  }

  override def isAllowedAfter(previous: Contract): Boolean = {
    previous match {
      case MisereContract =>
        true
      case _ =>
        false
    }
  }
}

case object PassContract extends Contract {
  override def isWhisterAllowed = false

  def isBidBetter(previous: Contract, allowSame: Boolean) = false

  override def isAllowedAfter(previous: Contract) = previous match {
    case PassContract =>
      false
    case _ =>
      true
  }
}

