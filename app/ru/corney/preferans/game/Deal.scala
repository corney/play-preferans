package ru.corney.preferans.game

import ru.corney.preferans.game.Place._

import scala.util.Random

/**
  * Created by dmitriikariaev on 5/27/16.
  */
case class Deal(dealer: Place, cards: Map[Place, Set[Card]], talon: Seq[Card]) {
  def apply(player: Place): Set[Card] = cards(player)
}

object Deal {
  def deal(dealer: Place, players: Set[Place]): Deal = {
    val shuffled: Seq[Card] = Random.shuffle(Deck.Cards)

    val one = next(dealer, players)
    val two = next(one, players)
    val three = next(two, players)
    Deal(
      dealer,
      Map(
        one -> shuffled.slice(0, 10).toSet,
        two -> shuffled.slice(10, 20).toSet,
        three -> shuffled.slice(20, 30).toSet
      ),
      shuffled.slice(30, 32)
    )
  }
}
