package ru.corney.preferans.game


import ru.corney.preferans.game.Suit.Suit

import scala.util.{Failure, Random, Success, Try}

/**
  * Created by dmitriikariaev on 5/25/16.
  */



import ru.corney.preferans.game.Place._


object TrickPlay {

  def max(cards: Map[Place, Card], trump: Option[Suit]): (Place, Card) = {
    cards.reduceLeft((l, r) => max(l, r, trump))
  }

  def max(first: (Place, Card), second: (Place, Card), trump: Option[Suit]): (Place, Card) = {

    if (first._2.suit == second._2.suit) {
      if (first._2.rank > second._2.rank) first else second
    } else {
      if (trump.contains(second._2.suit)) second else first
    }
  }
}

case class TrickPlayState(leading: Place, cards: Map[Place, Set[Card]], tricks: Map[Place, Int], played: Seq[Card])

/**
  *
  * @param dealer
  * @param cards
  * @param trump
  */
abstract class TrickPlay(dealer: Place, cards: Map[Place, Set[Card]], trump: Option[Suit]) {

  /**
    * Participants of this game
    */
  val players = cards.keySet

  protected var _history = List(
    initialState
  )

  protected def initialState: TrickPlayState = TrickPlayState(
    leading = next(dealer, players),
    cards = cards,
    tricks = players.map(place => (place, 0)).toMap,
    played = Seq.empty
  )

  /**
    * TODO выяснить устойчивое выражение "бросок карты". Сейчас это toss
    *
    * @param toss
    * @return Play round if cards are acceptable
    */
  def round(toss: Map[Place, Card]): Try[Place] = {
    if (isAllowed(toss)) {
      Success(doRound(toss))
    } else {
      Failure(new IllegalStateException("Wrong card set"))
    }
  }

  protected def doRound(toss: Map[Place, Card]): Place = {
    val (winner, _) = TrickPlay.max(toss, trump)

    _history :+= TrickPlayState(
      leading = winner,
      cards = state.cards.map {
        case (place, set) => (place, set.filter(card => card != toss(place)))
      },
      tricks = state.tricks.updated(winner, state.tricks(winner) + 1),
      played = toss.toSeq.map(_._2) ++ state.played
    )
    winner
  }

  /**
    *
    * @param toss
    * @return Verify if current cards allowed for next round
    */
  def isAllowed(toss: Map[Place, Card]): Boolean = {
    _isAllowed(toss, toss(leading).suit)
  }

  protected def _isAllowed(toss: Map[Place, Card], suit: Suit): Boolean = {
    // Карты должны присутствовать у игрока и соответствовать масти, в которую совершается выход
    // За исключением ситуации, когда у игрока нет ни данной масти, ни козырей
    toss.forall {
      case (place, card) => state.cards.get(place).exists { set =>
        set.contains(card) &&
          (
            card.suit == suit ||
              trump.contains(card.suit) ||
              set.forall(card => card.suit != suit && ! trump.contains(card.suit))
            )
      }
    }
  }

  /**
    *
    * @return History of current game
    */
  def history: Seq[TrickPlayState] = _history

  /**
    *
    * @return the current game status
    */
  def state: TrickPlayState = _history.last

  /**
    *
    * @return a Place of first hand in the next round
    */
  def leading: Place = state.leading

  /**
    *
    * @return true if game comleted
    */
  def isComplete: Boolean = state.cards.isEmpty

}



