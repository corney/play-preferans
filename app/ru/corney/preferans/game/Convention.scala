package ru.corney.preferans.game

/**
  * Created by dmitriikariaev on 5/30/16.
  */
case class Convention(
                       // Сборник конвенций для распасов
                       allPass: AllPassConvention,
                       bidConvention: BidConvention

                     )

case class BidConvention(
                          // Разрешен "здесь" через пасующего, или нет
                          allowHere: Boolean
                        )
