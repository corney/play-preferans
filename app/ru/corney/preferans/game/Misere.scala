package ru.corney.preferans.game

import ru.corney.preferans.game.Place.Place

object Misere {
  def apply(
             dealer: Place,
             declarer: Place,
             cards: Map[Place, Set[Card]]): Misere =
    new Misere(dealer, declarer, cards)
}

/**
  * Created by dmitriikariaev on 5/27/16.
  */
class Misere (dealer: Place, val declarer: Place, cards: Map[Place, Set[Card]]) extends TrickPlay(dealer, cards, None) {

}
