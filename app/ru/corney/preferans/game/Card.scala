package ru.corney.preferans.game


/**
  * Created by dmitriikariaev on 5/25/16.
  */
object Suit extends Enumeration {
  type Suit = Value
  val SPADES, CLUBS, DIAMONDS, HEARTS = Value
}

object Rank extends Enumeration {
  type Rank = Value
  val SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE = Value
}

case class Card(suit: Suit.Suit, rank: Rank.Rank)

object Card {
  implicit def CardOrdering: Ordering[Card] = Ordering.by(card => (card.suit, card.rank))
}


object Deck {
  val Cards = for (suit <- Suit.values.toSeq; rank <- Rank.values.toSeq) yield Card(suit, rank)
}