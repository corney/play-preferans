package ru.corney.preferans.game

import ru.corney.preferans.game.Place.Place
import ru.corney.preferans.game.Suit.Suit
import ru.corney.preferans.model.Person

/**
  * Created by dmitriikariaev on 5/27/16.
  */

object Place extends Enumeration {
  type Place = Value
  lazy val next = {
    val list = values.toList
    list.zip(list.tail :+ list.head).toMap
  }

  def next(current: Place, players: Set[Place]): Place = {
    val next = Place.next(current)
    if (players.contains(next)) next else Place.next(next)
  }

  val NORTH, EAST, SOUTH, WEST = Value
}

sealed class Game(val players: Map[Place, Person], convention: Convention) {

}

object Game {
  def apply(players: Map[Place, Person], convention: Convention): Game = new Game(players, convention)
}

