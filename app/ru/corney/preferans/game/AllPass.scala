package ru.corney.preferans.game

import ru.corney.preferans.game.Place.Place

/**
  * Created by dmitriikariaev on 5/27/16.
  */

case class AllPassConvention(dealerIsAllowedToGetTricks: Boolean)

object AllPassState extends Enumeration {
  type AllPassState = Value
  val FirstStep, SecondStep, ThirdStep, OtherSteps = Value
}

object AllPass {
  def apply(
             dealer: Place,
             cards: Map[Place, Set[Card]],
             talon: Seq[Card]
           )
           (
             implicit convention: AllPassConvention
           ): AllPass = {
    if (convention.dealerIsAllowedToGetTricks)
      new AllPassWithTricksToDealer(dealer, cards, talon)
    else
      new AllPassWithoutTricksToDealer(dealer, cards, talon)
  }
}
import Place._
abstract class AllPass(
               dealer: Place,
               cards: Map[Place, Set[Card]],
               talon: Seq[Card]
             )

  extends TrickPlay(dealer, cards, None) {

  import AllPassState._


  protected var lifecycleState = FirstStep

  override def isAllowed(toss: Map[Place, Card]) = {
    lifecycleState match {
      case FirstStep =>
        super._isAllowed(toss, talon.head.suit)
      case SecondStep =>
        super._isAllowed(toss, talon.last.suit)
      case _ =>
        super.isAllowed(toss)
    }
  }

  override def doRound(toss: Map[Place, Card]) = {
    lifecycleState match {
      case FirstStep =>
        lifecycleState = SecondStep
        super.doRound(prepareTossForFistStep(toss))
      case SecondStep =>
        lifecycleState = ThirdStep
        super.doRound(prepareTossForSecondStep(toss))
      case _ =>
        lifecycleState = OtherSteps
        super.doRound(toss)
    }
  }

  protected def prepareTossForFistStep(toss: Map[Place, Card]): Map[Place, Card]
  protected def prepareTossForSecondStep(toss: Map[Place, Card]): Map[Place, Card]


  override def leading: Place = {
    lifecycleState match {
      case (FirstStep | SecondStep) =>
        dealer
      case ThirdStep =>
        next(dealer, players)
      case _ =>
        super.leading
    }
  }
}


class AllPassWithTricksToDealer(
                        dealer: Place,
                        cards: Map[Place, Set[Card]],
                        talon: Seq[Card]
                      )
  extends AllPass(dealer, cards, talon) {


  override def initialState =
      TrickPlayState(
        leading = next(dealer, players),
        cards = cards,
        tricks = (players + dealer).map(place => (place, 0)).toMap,
        played = Seq.empty
      )

  override def prepareTossForFistStep(toss: Map[Place, Card]) = toss.updated(dealer, talon.head)

  override def prepareTossForSecondStep(toss: Map[Place, Card]) = toss.updated(dealer, talon.last)
}

class AllPassWithoutTricksToDealer(
                                 dealer: Place,
                                 cards: Map[Place, Set[Card]],
                                 talon: Seq[Card]
                               )
  extends AllPass(dealer, cards, talon) {


  override def prepareTossForFistStep(toss: Map[Place, Card]) = toss

  override def prepareTossForSecondStep(toss: Map[Place, Card]) = toss
}

