package ru.corney.preferans.dao

import java.util.UUID
import javax.inject._

import com.google.inject.ImplementedBy
import play.api.libs.json.Json
import play.modules.reactivemongo.ReactiveMongoApi
import play.modules.reactivemongo.json._
import reactivemongo.api.commands.WriteResult
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json.collection.JSONCollection
import ru.corney.preferans.model.Person

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by dmitriikariaev on 6/24/16.
  */
@ImplementedBy(classOf[PersonMongoRepo])
trait PersonRepo {
  def generateId(): String

  def find(id: String): Future[Option[Person]]

  def find(): Future[List[Person]]

  def save(person: Person): Future[Person]

  def delete(person: Person): Future[Unit]

  def delete(id: String): Future[Unit]

}

class PersonMongoRepo @Inject()(reactiveMongoApi: ReactiveMongoApi)(implicit ec: ExecutionContext) extends PersonRepo {

  def find(id: String) = {
    val query = Json.obj(Person.FieldID -> id)
    val found = collection.map(_.find(query).cursor[Person]())
    found.flatMap(_.headOption)
  }

  protected def collection = reactiveMongoApi.database.map(_.collection[JSONCollection]("persons"))

  def find() = {
    val query = Json.obj()
    val found = collection.map(_.find(query).cursor[Person]())
    found.flatMap(_.collect[List]())
  }


  def save(person: Person) = {
    val (id:String, personWithId) = person.id match {
      case Some(existedId) =>
        (existedId, person)
      case None =>
        val generatedId = generateId()
        (generatedId, person.copy(id = Some(generatedId)))
    }

    val query = Json.obj(Person.FieldID -> id)
    collection.flatMap(_.update(query, personWithId, upsert = true)).flatMap(result => {
      if (result.ok) {
        Future.successful(personWithId)
      } else {
        Future.failed(new IllegalStateException(result.errmsg.getOrElse("Unknown error")))
      }
    })
  }

  def delete(id: String) = {
    val query = Json.obj(Person.FieldID -> id)
    collection.flatMap(_.remove(query, firstMatchOnly = true)).flatMap(result => {
      if (result.ok) {
        Future.successful(Unit)
      } else {
        Future.failed(new IllegalStateException(result.errmsg.getOrElse("Unknown error")))
      }
    })
  }

  def delete(person: Person) = delete(person.id.get)

  def generateId() = BSONObjectID.generate.stringify


}
