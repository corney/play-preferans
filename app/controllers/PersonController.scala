package controllers

import java.util.UUID
import javax.inject._

import akka.stream.Materializer
import play.api._
import play.api.mvc._
import play.api.i18n._
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.libs.json.{JsObject, Json}
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.play.json.collection.JSONCollection
import ru.corney.preferans.model.Person, Person._
import scala.concurrent.{ExecutionContext, Future}

import play.modules.reactivemongo.json._
import play.modules.reactivemongo.json.collection._

@Singleton
class PersonController @Inject() (val reactiveMongoApi: ReactiveMongoApi, val messagesApi: MessagesApi)
                                 (implicit ec: ExecutionContext, implicit val materializer: Materializer)
  extends Controller with MongoController with ReactiveMongoComponents with I18nSupport {

  val personForm: Form[CreatePersonForm] = Form {
    mapping(
      "name" -> nonEmptyText,
      "login" -> nonEmptyText,
      "password" -> nonEmptyText
    )(CreatePersonForm.apply)(CreatePersonForm.unapply)
  }

  def index = Action {
    Ok(views.html.person(personForm))
  }

  def collection = reactiveMongoApi.database.map(_.collection[JSONCollection]("persons"))

  def addPerson() = Action.async { implicit request =>

    personForm.bindFromRequest.fold(
      errorForm => {
        Future.successful(Ok(views.html.person(errorForm)))
      },
      person => {
        collection.flatMap(_.insert(Person(Some(UUID.randomUUID().toString), person.login, person.password, person.name))).map (_=>
          Redirect(routes.PersonController.index())
        )
      }
    )
  }

  def getPersons = Action.async {implicit request =>

    val query = Json.obj()
    val found = collection.map(_.find(query).cursor[Person]())

    found.flatMap(_.collect[List]()).map { people =>
      Ok(Json.toJson(people))
    }
  }
}

case class CreatePersonForm(login: String, password: String, name: String)